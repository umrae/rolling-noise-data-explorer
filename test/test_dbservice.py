import pytest

from datetime import datetime
from src.dao.measurement_filters import MeasurementFilters
from src.dao.measurement_type import MeasurementType
from src.dao.surface_category import SurfaceCategory
from src.dao.surface_type import SurfaceType
from src.dao.vehicle_category import VehicleCategory
from src.dto import db_service, HIDDEN_SURFACE_IDS


@pytest.fixture
def mock_filters():
    """
    Mock measurement filters
    """
    yield MeasurementFilters(
        measurement_type=MeasurementType.VI,
        surface_types=[
            SurfaceType(id=19, name="BBTM 0/6 classe 2", category=SurfaceCategory.R1)
        ],
        vehicle_categories=[VehicleCategory(id=2, name="VL", longname="VL")],
        reference_speed=90,
        surface_age_max_years=20,
        surface_age_min_years=0,
        campaign_date_min=datetime.fromtimestamp(0),
        campaign_date_max=datetime.now(),
        include_corrected_measurements=False,
    )


def test_connect():
    """
    Tests establishing a connection with the database.
    """
    connection = db_service.create_db_connection()
    assert connection is not None


def test_get_oldest_measurement_date():
    """
    Tries fetching the oldest measurement to date in the database
    """
    date = db_service.get_oldest_measurement_date()
    assert date > datetime.fromtimestamp(0)


def test_get_latest_measurement_date():
    """
    Tries fetching the latest measurement to date in the database
    """
    date = db_service.get_latest_measurement_date()
    assert date > datetime.fromtimestamp(0)


def test_compare_latest_oldest_measurements():
    """
    Checks if latest and oldest measurements are different
    """
    oldest = db_service.get_oldest_measurement_date()
    latest = db_service.get_latest_measurement_date()
    assert latest > oldest


def test_get_vehicle_categories(mock_filters):
    """
    Tests fetching vehicle categories

    Args:
        mock_filters (_type_): Mock filters
    """
    vehicle_categories = db_service.get_vehicle_categories_for_filters(mock_filters)
    assert len(vehicle_categories) > 0


def test_get_available_reference_speeds(mock_filters):
    """
    Tests fetching available reference speeds

    Args:
        mock_filters (_type_): Mock filters
    """
    reference_speeds = db_service.get_available_reference_speeds_for_filters(
        mock_filters
    )
    assert len(reference_speeds) > 0


def test_get_measurements(mock_filters):
    """
    Tests fetching some measurements

    Args:
        mock_filters (_type_): Mock filters
    """
    measurements = db_service.get_measurements_for_filters(mock_filters)
    assert len(measurements) > 0
    for measurement in measurements:
        assert measurement.date >= mock_filters.campaign_date_min.date()
        assert measurement.date <= mock_filters.campaign_date_max.date()
        assert measurement.reference_speed == mock_filters.reference_speed
        assert measurement.surface_age >= mock_filters.surface_age_min_years
        assert measurement.surface_age <= mock_filters.surface_age_max_years
        assert measurement.la_max > 0


def test_hidden_surface_ids():
    """
    Tests that hidden surface ids are not present when getting all surface types
    """
    surface_types = db_service.get_all_surface_types()
    assert len(surface_types) > 0
    for surface_type in surface_types:
        assert surface_type.id not in HIDDEN_SURFACE_IDS
