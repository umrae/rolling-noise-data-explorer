#### Références bibliographiques

##### Normes de mesures

- Norme [NF EN ISO 11819-1](https://www.boutique.afnor.org/fr-fr/norme/nf-en-iso-118191/acoustique-mesurage-de-linfluence-des-revetements-de-chaussees-sur-le-bruit/fa196315/344254), Acoustique - Mesurage de l'influence des revêtements de chaussées sur le bruit émis par la circulation - Partie 1 : méthode statistique au passage, 2023, AFNOR Editions
- Norme [NF EN ISO 11819-2](https://www.boutique.afnor.org/fr-fr/norme/nf-en-iso-118192/acoustique-methode-de-mesurage-de-linfluence-des-revetements-de-chaussees-s/fa132448/81008), Acoustique - Méthode de mesurage de l'influence des revêtements de chaussées sur le bruit émis par la circulation - Partie 2 : méthode de proximité immédiate, 2018, AFNOR Editions.
- Norme [XP ISO/TS 11819-3](https://www.boutique.afnor.org/fr-fr/norme/xp-iso-ts-118193/acoustique-methode-de-mesurage-de-linfluence-des-revetements-de-chaussees-s/fa201829/238490), Acoustique - Méthode de mesurage de l'influence des revêtements de chaussées sur le bruit émis par la circulation - Partie 3 : pneumatiques de référence, 2021, AFNOR Editions.


##### Informations sur le bruit de roulement

- [Guide](https://www.idrrim.com/ressources/documents/9/7535-IDRRIM_Guide-bruit-de-roulement.pdf) Bruit de roulement : État de l'art et recommandations, mai 2020, IDRRIM.

_Méthode NMP08 :_

- [Prévision du bruit routier - Tome 1-Calcul des émissions sonores dues au trafic routier](https://www.cerema.fr/fr/centre-ressources/boutique/prevision-du-bruit-routier-calcul-emissions-sonores-dues-au?v=5628) SETRA, 2009, 124p. ISBN 555-2-00-054432-9
- Hamet JF, Besnard F., Doisy S., Lelong J., le Duc E., [New vehicle noise emission for French traffic noise prediction](https://doi.org/10.1016/j.apacoust.2010.05.003), Applied Acoustics (71)9, 2010, 861-869.


#### Crédits

- Base de données : [UMRAE](https://www.umrae.fr), [Cerema](https://www.cerema.fr/fr) Strasbourg
- Développement logiciel (application shiny): Marceau Tonelli ([UMRAE](https://www.umrae.fr), [Cerema](https://www.cerema.fr/fr)), 2024.
- Conception logiciel (application shiny): Marceau Tonelli, David Ecotière ([UMRAE](https://www.umrae.fr), [Cerema](https://www.cerema.fr/fr))
