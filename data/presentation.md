#### Présentation

Cette application permet de visualiser les performances acoustiques de différents revêtements de chaussée routiers utilisés en France. Elle présente un indicateur sonore obtenu selon différentes méthodes ou procédures de mesure :

- Méthode de mesure au passage (CPB), procédure «Véhicule isolé» (VI) : NF EN ISO 11819-1
- Méthode de mesure au passage (CPB), procédure «Véhicule maitrisé » (VM) : NF EN ISO 11819-2
- Méthode mesure en continu (CPX) : NF EN ISO 11819- 2 et XP ISO/TS 11819-3

La description de ces méthodes est fournie par exemple dans le guide IDRRIM (voir l’onglet Informations).


#### Résultats

L'onglet Résultats présente pour chaque catégorie de revêtement la distribution statistique de l’indicateur sonore du bruit de roulement sous la forme d’une boite à moustaches (diagramme en boîte, box plot ou boîte de Tukey). Ces diagrammes font apparaitre en particulier la valeur moyenne, la valeur médiane, le premier et troisième quartile statistique (Q1 et Q3) de la distribution.

L'onglet Informations fournit des références bibliographiques concernant la mesure du bruit de roulement.

#### Interprétation des résultats, précautions d'utilisation

Les méthodes CPB (VI/VM) et la méthode CPX ne sont pas équivalentes et les indicateurs sonores sont significativement différents, ces méthodes sont cependant complémentaires. Les mesures VI/VM sont représentatives des caractéristiques sonores de la chaussée au droit de la mesure et intègrent également la diversité du parc roulant. Les mesures CPX permettent d’évaluer les propriétés acoustiques du revêtement de chaussée seul et intègre les éventuelles inhomogénéités spatiales des propriétés acoustiques le long du tronçon mesuré.

Une différence d’indicateur sonore entre deux catégories de revêtement n'est perceptivement significative que si elle est supérieure à 2dBA en valeur absolue, car l’audition humaine ne perçoit généralement pas de différence entre deux sons dont l’écart d’intensité sonore est inférieur à ce seuil.

Les indicateurs sonores présentés ne représentent pas l’émission sonore totale d’un trafic routier, mais uniquement la contribution du bruit de roulement d’un véhicule isolé. L’émission sonore totale d’un trafic routier doit également intégrer la contribution du bruit moteur et des propriétés du trafic. Celle-ci peut être évaluée, selon la méthode NMPB08, à l’aide de l’application [MOTOR](https://cerema-med.shinyapps.io/RoadNoiseEmNMPB2008/).

Les indicateurs sonores présentés sont une image de l’émission sonore du bruit de roulement pour chaque catégorie de revêtement. Ils ne représentent pas le niveau sonore perçu au niveau d’un riverain de la voirie, qui dépend en plus du bruit moteur et de différents phénomènes d’atténuation lors de la propagation du bruit entre la voirie et le riverain.


#### Nomenclature générale des principaux revêtements

- BBUM = Béton Bitumineux Ultra Mince.
- BBTM = Béton Bitumineux Très Mince.
- BBDr = Béton Bitumineux Drainant.
- BBSG = Béton Bitumineux Semi-Grenu.
- ECF ou MBCF = Enrobé Coulé à Froid ou Matériaux Bitumineux Coulés à Froid.
- BC = Béton de Ciment.
- ES = Enduit Superficiel.

#### Classification des revêtements selon la méthode NMPB08

- R1 : catégorie revêtements moins bruyants = BBUM 0/6, BBDr 0/10, BBTM 0/6-type2, BBTM 0/6 type 1, BBTM 0/10-type2.
- R2 : catégorie revêtements intermédiaires = BBSG 0/10, BBTM 0/10-type1, BBUM 0/10 et ECF.
- R3 : catégorie revêtements plus bruyants = BBSG 0/14, BBTM 0/14, ES 6/10, BC et ES 10/14.

Les revêtements classés ‘UNDEFINED’ sont ceux qui n’ont pas fait l’objet de classification lors de la publication de la méthode NMPB08.
