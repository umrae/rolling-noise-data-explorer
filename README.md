# Rolling noise database explorer

A [Shiny python](https://shiny.posit.co/py) app that allows exploring rolling noise measurements made by the UMRAE.

_TODO: Add app link once deployed_

## Description

This project aims to give a user friendly access to the measurements stored in the database. Measurements can be filtered by measurement type (VI, VM or CPX), vehicle category (light or heavy vehicles), reference speed on which the measurements were made as well as the road surface type it was measured on.

The measurements are then displayed in a box plot to show the variety of values measured on each road surface type.

### Technical details

This app is built using Shiny for python. The measurements are stored in an SQLite database, and accessed using `sqlite3` along with `pypika` for query building. Plots are generated using `plotly`.

## Setup locally

It is strongly recommended to use a python virtual environment to manage dependencies on a per project basis. To learn how to setup and create a virtual environment, check [this guide](https://docs.python.org/3/library/venv.html). Once your virtual environment is created and activated, install the project's dependencies by running:

```sh
pip install -r requirements.txt
```

Once the dependencies are successfully installed, you can run the app by running the following commands:

```python
$ python # Start the python interpreter

>>> from shiny import run_app
>>> run_app("src.app:app")
```

You should now be able to access your app by opening the URL shown in the shiny server logs.

### Development tips

It is way simpler however to use [VSCode](https://code.visualstudio.com/download) with the [Shiny extension](https://marketplace.visualstudio.com/items?itemName=Posit.shiny-python) for developing shiny python apps. This will allow you to start the app just by cliking a button and see your changes in real time as it will auto reload every time you save a file.

#### Unit tests

Some unit tests can be found under the `test` folder. All the tests must pass in order for the CI pipeline to proceed with deploying a new version of the app. To run the tests locally, simply run the following command in your terminal:

```sh
pytest -vv -s
```

**If you see some error, you should fix it before pushing your changes.**

#### Linter

The code is linted with [Ruff linter](https://github.com/astral-sh/ruff). Linter is also ran as part of the test pipeline, so if there are some error the pipeline will not succeed, hence also blocking the deployment job. If you're using VSCode, download the Ruff extension to see linter warnings beforehand. If you're more of a manual person, you can run the linter with this command:

```sh
ruff check src/ test/
```

To fix linter errors automatically, simply add `--fix` to this command.

## Update the database

The database itself is originally stored as an SQL script that creates and populates the database with records. To keep things simple and avoid hosting the database on a separate server, we're using SQLite to access the database locally from the app. The SQLite database is then pushed to the Shinyapps server with the rest of the app.

To update the SQLite database, either modify it directly and replace `rolling_noise.db` with a new version, or use [`mysql-to-sqlite3`](https://pypi.org/project/mysql-to-sqlite3/) to make a new SQLite database from a local MySQL server (i.e. Wamp). It can be done like so:

```sh
mysql2sqlite --sqlite-file data/rolling_noise.db \
             --mysql-database bddbruit \
             --mysql-user root \
             --prompt-mysql-password
```

Note that this command is an example but the values of parameters may vary based on your environment. If this command succeeds without issues, the `rolling_noise.db` file should be up to date with your MySQL database.

### Updating the databse manually

The simplest way to open the database is to open it with an SQLite editing tool such as [DB Explorer for SQLite](https://sqlitebrowser.org/). Make your modifications here and save the database. Once this is done, redeploy the application following the steps listed above.

### :warning: Important:

- Do not forget to commit and push your changes once they are done, otherwise someone else editing the database won't be able to do it on top of your changes and they will be overriden by the next deployment.
- Similarly, do not forget to pull the latest changes before making yours, otherwise you may have some bad surprises when it will be time to push!
- Lastly, think twice before making any change to the database's structure, because renaming or deleting columns, or changing relations might break the application! Always check your changes locally before deploying.


## Deploying to Shinyapps

There are two deployment environments: staging (`mtonelli-umrae.shinyapps.io`) and production (`cerema-med.shinyapps.io`). We are using Gitlab CI/CD to automatically deploy new versions whenever a new push is made to the `dev` branch (deploys to staging) or to the `master` branch (deploys to production). Unless you need to deploy something while being unable to access this git repository, it is best to stick with automatic deployments for reliability reasons.

### Manually deploy a new version

If you're in need to manually deploy a new version of this app to a Shinyapps server, use [`rsconnect` for python](https://pypi.org/project/rsconnect-python/). Add the server you want to deploy to by running:

```sh
rsconnect add --account [account-name] \
              --name [account-name] \
              --token [server-token] \
              --secret [server-secret]
```

Once added successfully, you can deploy a new version of the app simply like so:

```sh
rsconnect deploy shiny ./ \
          --entrypoint src.app:app
          --name [account-name]
          --app-id [app-id]
```

The app identifier can be found in the Shinyapps dashboard. If you want to deploy the app to a new URL, use the `--title` flag with the title you want to give to your new app.
