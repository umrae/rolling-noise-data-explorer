from enum import Enum


class MeasurementType(Enum):
    """
    Enum listing all the measurement types that can be fetched
    """

    VI = "VI"
    VM = "VM"
    VI_VM = "VI/VM"
    CPX = "CPX"
