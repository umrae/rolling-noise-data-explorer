from dataclasses import dataclass
from src.dao.surface_category import SurfaceCategory


@dataclass
class SurfaceType:
    """
    Road surface type with name and unique identifier.
    Every road surface in the database should be linked with a surface type.
    """

    id: int
    name: str
    category: SurfaceCategory
