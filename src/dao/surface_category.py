from enum import Enum


class SurfaceCategory(Enum):
    """
    Category of surface, basically a rating of how good it is acoustically speaking.
    R1 is best, R3 is worst.
    """

    UNDEFINED = 0
    R1 = 1
    R2 = 2
    R3 = 3

    @property
    def plot_line_color(self) -> str:
        match self:
            case SurfaceCategory.UNDEFINED:
                return "rgb(37, 68, 65)"
            case SurfaceCategory.R1:
                return "rgb(67, 170, 139)"
            case SurfaceCategory.R2:
                return "rgb(0, 117, 196)"
            case SurfaceCategory.R3:
                return "rgb(214, 81, 8)"

    @property
    def plot_fill_color(self) -> str:
        match self:
            case SurfaceCategory.UNDEFINED:
                return "rgba(37, 68, 65, 0.25)"
            case SurfaceCategory.R1:
                return "rgba(67, 170, 139, 0.25)"
            case SurfaceCategory.R2:
                return "rgba(0, 117, 196, 0.25)"
            case SurfaceCategory.R3:
                return "rgba(214, 81, 8, 0.25)"
