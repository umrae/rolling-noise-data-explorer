from dataclasses import dataclass
from datetime import date

from src.dao.surface_type import SurfaceType


@dataclass
class Measurement:
    """
    Describes a noise measurement as defined in the database
    """

    la_max: float
    """Measured value"""
    surface_type: SurfaceType
    """Surface the measurement was made on"""
    surface_age: int
    """Age of the surface in years at the time the measurement was made"""
    reference_speed: int
    """Reference speed for the measured LAmax value"""
    regression_coeff: float
    """Regression coefficient used for the calculation of LAmax at reference speed"""
    date: date
    """Date at which the measurement was made"""
