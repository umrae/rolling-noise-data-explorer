from dataclasses import dataclass


@dataclass
class VehicleCategory:
    """
    Describes a vehicle category with a unique identifier and name
    """

    id: int
    name: str
    longname: str
