from dataclasses import dataclass
from datetime import datetime
from src.dao.surface_type import SurfaceType
from src.dao.measurement_type import MeasurementType
from src.dao.vehicle_category import VehicleCategory


@dataclass
class MeasurementFilters:
    """
    Filters that can be used to query rolling noise measurements.
    """

    measurement_type: MeasurementType
    surface_types: list[SurfaceType]
    vehicle_categories: list[VehicleCategory]
    reference_speed: int

    surface_age_min_years: int
    """Minimum age of road surface on the day the measurement was made, expressed in years"""
    surface_age_max_years: int
    """Maximum age of road surface on the day the measurement was made, expressed in years"""

    campaign_date_min: datetime
    """Minimum date of measurement campaign"""
    campaign_date_max: datetime
    """Maximum date of measurement campaign"""

    include_corrected_measurements: bool
    """
    If set to true, the measurements for which the provided reference speed is between their
    max and min speed values will also be included. Their LAmax value will then be corrected using
    the regression coefficient stored in the database.
    """

    def get_surface_type_names(self) -> list[str]:
        """
        Gets the list of surface type names

        Returns:
            list[str]: Surface type names as a list
        """
        return [s.name for s in self.surface_types]
