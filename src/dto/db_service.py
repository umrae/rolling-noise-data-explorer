from datetime import datetime
from sqlite3 import connect, Connection
from pypika import SQLLiteQuery, Table, Tables, functions, Order

from src.dao.measurement_filters import MeasurementFilters
from src.dao.surface_category import SurfaceCategory
from src.dao.vehicle_category import VehicleCategory
from src.dao.measurement import Measurement
from src.dao.measurement_type import MeasurementType
from src.dao.surface_type import SurfaceType
from src.definitions import DATA_ROOT, DB_NAME
from src.dto import HIDDEN_SURFACE_IDS


def create_db_connection() -> Connection:
    """
    Creates an SQLite3 connection with the noise database

    Returns:
        Connection: SQLite3 connection object
    """
    # Create a connection with the database, implicitely creating it if it doesn't yet exists
    return connect(f"{DATA_ROOT}/{DB_NAME}")


def apply_vehicle_category_filters_to_query(filters: MeasurementFilters, query):
    """
    Filters the query results by vehicle category, based on measurement type

    Args:
        filters (MeasurementFilters): A set of filters to apply
        query (QueryBuilder): Pypika query to apply filters on top of
    """

    def _apply_vehicle_category_filters_vi(filters: MeasurementFilters, query):
        # For VI measurements, join the mesures_vi table then filter by vehicle category
        measurements, measurements_vi = Tables("mesure", "mesures_vi")
        return (
            query.inner_join(measurements_vi)
            .on(measurements_vi.no_mesure == measurements.no_mesure)
            .where(
                measurements_vi.no_categorie_vehicule.isin(
                    [v.id for v in filters.vehicle_categories]
                )
            )
        )

    def _apply_vehicle_category_filters_vm(filters: MeasurementFilters, query):
        # For VM measurements, join the mesures_vm and effectuer_sur tables then
        # filter by vehicle category
        measurements, measurements_vm, done_on = Tables(
            "mesure", "mesures_vm", "effectuer_sur"
        )
        return (
            query.inner_join(measurements_vm)
            .on(measurements_vm.no_mesure == measurements.no_mesure)
            .inner_join(done_on)
            .on(measurements_vm.no_mes_vm == done_on.no_mes_vm)
            .where(
                done_on.categorie_vehicule.isin(
                    [v.id for v in filters.vehicle_categories]
                )
            )
        )

    # Filters will differ based on the measurement type because data structure will be different
    match filters.measurement_type:
        case MeasurementType.VI:
            return _apply_vehicle_category_filters_vi(filters, query)
        case MeasurementType.VM:
            return _apply_vehicle_category_filters_vm(filters, query)
        case MeasurementType.VI_VM:
            # If we want both VI and VM, create a UNION query that will gather results from both VI and VM tables
            query = _apply_vehicle_category_filters_vi(
                filters, query
            ) * _apply_vehicle_category_filters_vm(filters, query)
            # By default Pypika wraps select operations in parenthesis when creating union queries but SQLite
            # does not accept this syntax so we need to disable this feature. See https://github.com/kayak/pypika/pull/746
            query.base_query.wrap_set_operation_queries = False
            return query
        case MeasurementType.CPX:
            # For CPX measurements, we don't care about vehicle category since all measurements are done on the same trailer
            return query


def apply_reference_speed_filters_to_query(filters: MeasurementFilters, query):
    """
    Filters query results by reference speed, based on measurement type

    Args:
        filters (MeasurementFilters): A set of filters to apply
        query (QueryBuilder): Pypika query to apply filters on top of
    """
    match filters.measurement_type:
        case MeasurementType.VI | MeasurementType.VM | MeasurementType.VI_VM:
            measurements = Table("mesure")
            if filters.include_corrected_measurements:
                return query.where(
                    (measurements.vit_ref == filters.reference_speed)
                    | (
                        (measurements.pente_gen.notnull())
                        & (measurements.vit_min.notnull())
                        & (measurements.vit_max.notnull())
                        & (filters.reference_speed > measurements.vit_min)
                        & (filters.reference_speed < measurements.vit_max)
                    )
                )
            else:
                return query.where(measurements.vit_ref == filters.reference_speed)
        case MeasurementType.CPX:
            # TODO: Can we find a reliable regression coefficient for CPX measurements?
            measurements, obtained_by, reference_speeds = Tables(
                "cpx", "est_obtenue_par", "vitesse_ref"
            )
            return (
                query.inner_join(obtained_by)
                .on(measurements.no_mes_cpx == obtained_by.no_mes_cpx)
                .inner_join(reference_speeds)
                .on(obtained_by.no_vitesse == reference_speeds.no_vitesse)
                .where(reference_speeds.type_vitesse == filters.reference_speed)
            )


def get_all_surface_types() -> list[SurfaceType]:
    """
    Queries all surface types in the database

    Returns:
        list[SurfaceType]: A list of SurfaceType objects
    """
    # Build SQL query
    surface_types = Table("type_revetement")
    query = (
        SQLLiteQuery.from_(surface_types)
        .select(
            surface_types.no_type_revet,
            surface_types.type_revet,
            surface_types.categorie_revet,
        )
        .where(surface_types.no_type_revet.notin(HIDDEN_SURFACE_IDS))
        .orderby(surface_types.type_revet, order=Order.asc)
    )
    # Execute query
    cursor.execute(query.get_sql())
    # Process results
    results = []
    for id, name, category in cursor:
        results.append(SurfaceType(id, name, SurfaceCategory(category)))
    return results


def get_available_surface_types_for_filters(
    filters: MeasurementFilters,
) -> list[SurfaceType]:
    """
    Queries all available surface types in the database that have at least one measurement
    available for the given filters

    Args:
        filters (MeasurementFilters): A set of filters

    Returns:
        list[SurfaceType]: A list of SurfaceType objects
    """
    # Build SQL query
    surface_types, surfaces, campaigns = Tables(
        "type_revetement",
        "revetement",
        "campagne",
    )
    # Shared base for all queries
    query = (
        SQLLiteQuery.from_(surfaces)
        .inner_join(campaigns)
        .on(surfaces.no_revet == campaigns.no_revet)
        .inner_join(surface_types)
        .on(surfaces.no_type_revet == surface_types.no_type_revet)
        .select(
            surface_types.no_type_revet,
            surface_types.type_revet,
            surface_types.categorie_revet,
        )
        .where(
            (campaigns.date_mesure - surfaces.date_mise_en_oeuvre)[
                filters.surface_age_min_years : filters.surface_age_max_years
            ]
        )
    )
    # Join measurement table to campaign table based on measurement type
    match filters.measurement_type:
        case MeasurementType.VI | MeasurementType.VM | MeasurementType.VI_VM:
            measurements = Table("mesure")
            query = query.inner_join(measurements).on(
                measurements.no_campagne == campaigns.no_campagne
            )
        case MeasurementType.CPX:
            measurements = Table("cpx")
            query = query.inner_join(measurements).on(
                campaigns.no_cpx == measurements.no_mes_cpx
            )
    # Apply input filters on top of that base query
    query = apply_reference_speed_filters_to_query(filters, query)
    query = apply_vehicle_category_filters_to_query(filters, query)
    # Wrap our query in a parent query so that the grouping and ordering are applied on all queries
    # in case the base query is a union (like in the case of VI+VM)
    query = (
        SQLLiteQuery.from_(query)
        .select(query.star)
        .groupby("no_type_revet")
        .orderby("type_revet", order=Order.asc)
    )
    # Execute query
    cursor.execute(query.get_sql())
    # Process results
    results = []
    for id, name, category in cursor:
        results.append(SurfaceType(id, name, SurfaceCategory(category)))
    return results


def get_available_reference_speeds_for_filters(
    filters: MeasurementFilters,
) -> list[int]:
    """
    Gets a list of possible reference speeds for the given filters

    Args:
        filters (MeasurementFilters): A set of measurement filters

    Returns:
        list[int]: Available reference speeds to chose from
    """
    # Build SQL query based on filters
    match filters.measurement_type:
        case MeasurementType.VI | MeasurementType.VM | MeasurementType.VI_VM:
            measurements = Table("mesure")
            query = (
                SQLLiteQuery.from_(measurements).select(measurements.vit_ref).distinct()
            )
            query = apply_vehicle_category_filters_to_query(filters, query).orderby(
                measurements.vit_ref, order=Order.asc
            )
        case MeasurementType.CPX:
            measurements, obtained_by, reference_speeds = Tables(
                "cpx", "est_obtenue_par", "vitesse_ref"
            )
            query = SQLLiteQuery.from_(measurements)
            query = apply_vehicle_category_filters_to_query(filters, query)
            query = (
                query.inner_join(obtained_by)
                .on(measurements.no_mes_cpx == obtained_by.no_mes_cpx)
                .inner_join(reference_speeds)
                .on(obtained_by.no_vitesse == reference_speeds.no_vitesse)
                .select(reference_speeds.type_vitesse)
                .orderby(reference_speeds.type_vitesse, order=Order.asc)
            )
    # Execute query
    cursor.execute(query.get_sql())
    # Process results
    results = []
    for entry in cursor:
        results.append(int(entry[0]))
    return results


def get_measurements_for_filters(filters: MeasurementFilters) -> list[Measurement]:
    """
    Looks for all the measurements matching the given filters in the database

    Args:
        filters (MeasurementFilters): A set of filters to apply

    Returns:
        list: All the Measurements matching the given filters
    """
    # Adapt selected properties based on measurement type
    match filters.measurement_type:
        case MeasurementType.VI | MeasurementType.VM | MeasurementType.VI_VM:
            surfaces, surface_types, measurements, campaigns = Tables(
                "revetement", "type_revetement", "mesure", "campagne"
            )
            query = (
                SQLLiteQuery.from_(measurements)
                .inner_join(campaigns)
                .on(measurements.no_campagne == campaigns.no_campagne)
                .inner_join(surfaces)
                .on(surfaces.no_revet == campaigns.no_revet)
                .inner_join(surface_types)
                .on(surface_types.no_type_revet == surfaces.no_type_revet)
                .select(
                    measurements.LAmax_vref_corr_t,
                    surface_types.type_revet,
                    surface_types.categorie_revet,
                    surface_types.no_type_revet,
                    campaigns.date_mesure - surfaces.date_mise_en_oeuvre,
                    measurements.vit_ref,
                    measurements.pente_gen,
                    campaigns.date_mesure,
                )
                .where(measurements.LAmax_vref_corr_t > 0)
            )
            group_by_key = measurements.no_mesure
        case MeasurementType.CPX:
            surfaces, surface_types, measurements, campaigns, gives, reference_speed = (
                Tables(
                    "revetement",
                    "type_revetement",
                    "cpx",
                    "campagne",
                    "donne",
                    "vitesse_ref",
                )
            )
            query = (
                SQLLiteQuery.from_(measurements)
                .inner_join(campaigns)
                .on(campaigns.no_cpx == measurements.no_mes_cpx)
                .inner_join(surfaces)
                .on(surfaces.no_revet == campaigns.no_revet)
                .inner_join(gives)
                .on(measurements.no_mes_cpx == gives.no_mes_cpx)
                .inner_join(surface_types)
                .on(surface_types.no_type_revet == surfaces.no_type_revet)
                .select(
                    gives.LPC_rev_lateral,
                    surface_types.type_revet,
                    surface_types.categorie_revet,
                    surface_types.no_type_revet,
                    campaigns.date_mesure - surfaces.date_mise_en_oeuvre,
                    reference_speed.type_vitesse,
                    0.0,
                    campaigns.date_mesure,
                )
                .where(gives.LPC_rev_lateral > 0)
            )
            group_by_key = measurements.no_mes_cpx
    # Add where clauses common to every measurement type
    query = (
        query.where(
            surface_types.no_type_revet.isin([s.id for s in filters.surface_types])
        )
        .where(
            (campaigns.date_mesure - surfaces.date_mise_en_oeuvre)[
                filters.surface_age_min_years : filters.surface_age_max_years
            ]
        )
        .where(
            campaigns.date_mesure[filters.campaign_date_min : filters.campaign_date_max]
        )
        .groupby(group_by_key)
    )
    # Apply category and speed filtering
    query = apply_reference_speed_filters_to_query(filters, query)
    query = apply_vehicle_category_filters_to_query(filters, query)
    # Execute query
    cursor.execute(query.get_sql())
    # Process results
    results = []
    for (
        la_max,
        surface_name,
        surface_category,
        surface_id,
        surface_age,
        reference_speed,
        regression_coeff,
        date,
    ) in cursor:
        results.append(
            Measurement(
                la_max,
                SurfaceType(
                    surface_id, surface_name, SurfaceCategory(surface_category)
                ),
                surface_age,
                reference_speed,
                regression_coeff,
                datetime.strptime(date, "%Y-%m-%d").date(),
            )
        )
    return results


def get_vehicle_categories_for_filters(
    filters: MeasurementFilters,
) -> dict[str, VehicleCategory]:
    """
    Fetches the available vehicle categories listed in the database and filter them out
    based on the selected measurement type

    Args:
        filters (MeasurementFilters): A set of measurement filters

    Returns:
        dict[str, VehicleCategory]: A dictionary of all available vehicle categories for the current filters,
                                    keyed by category name as it is used as id in the dropdown menu
    """
    # This is hardcoded for the sake of simplicity because the current database structure contains
    # too many exceptions to handle dynamically. If needed in the future, we can make it dynamic
    # using the vehicles categories table as reference.
    match filters.measurement_type:
        case MeasurementType.VI:
            # VI measurements are made on TR, VL and PL vehicles
            return {
                "VL": VehicleCategory(
                    id=2,
                    name="VL",
                    longname="VL",
                ),
                "PL": VehicleCategory(
                    id=3,
                    name="PL",
                    longname="PL (2 essieux)",
                ),
                "TR": VehicleCategory(
                    id=1,
                    name="TR",
                    longname="TR (3 essieux et plus)",
                ),
            }
        case MeasurementType.VM | MeasurementType.VI_VM | MeasurementType.CPX:
            # VM and CPX measurements are only made on VL vehicles
            return {
                "VL": VehicleCategory(
                    id=2,
                    name="VL",
                    longname="VL",
                ),
            }


def get_latest_measurement_date() -> datetime:
    """
    Gets the date of the latest measurement present in the database

    Returns:
        datetime: Latest measurement date
    """
    # Build query
    campaigns = Table("campagne")
    query = SQLLiteQuery.from_(campaigns).select(functions.Max(campaigns.date_mesure))
    # Execute query
    cursor.execute(query.get_sql())
    # This query returns a list of one single element tuple so we need to unpack it
    [(date,)] = cursor.fetchall()
    return datetime.strptime(date, "%Y-%m-%d")


def get_oldest_measurement_date() -> datetime:
    """
    Gets the date of the oldest measurement present in the database

    Returns:
        datetime: Oldest measurement date
    """
    # Build query
    campaigns = Table("campagne")
    query = (
        SQLLiteQuery.from_(campaigns)
        .select(functions.Min(campaigns.date_mesure))
        .where(campaigns.date_mesure > 0)
        .where(campaigns.date_mesure < datetime.now())
    )
    # Execute query
    cursor.execute(query.get_sql())
    # This query returns a list of one single element tuple so we need to unpack it
    [(date,)] = cursor.fetchall()
    return datetime.strptime(date, "%Y-%m-%d")


def close_db_connection():
    """
    Closes the previously created MySQL connection and cursor objects
    """
    if cursor:
        cursor.close()
    if connection:
        connection.close()


# Open a database connection
connection = create_db_connection()
# Get a cursor from that connection to perform operations
cursor = connection.cursor()
