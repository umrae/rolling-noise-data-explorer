"""
This file contains constants used in database service
"""

HIDDEN_SURFACE_IDS = [
    27,  # Colgrip
]
"""
Surfaces that should be hidden to the end user, either because measurements are not
trustworthy or because they are attached to sensitive data that should not be revealed
to the end users.
"""
