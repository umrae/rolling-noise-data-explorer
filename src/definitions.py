import os.path

SRC_ROOT = os.path.dirname(os.path.abspath(__file__))
DATA_ROOT = os.path.dirname(SRC_ROOT) + "/data"
DB_NAME = "rolling_noise.db"
