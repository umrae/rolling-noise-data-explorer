from plotly.graph_objs._figure import Figure
from shiny import reactive, ui, render
from shinywidgets import render_widget
from src.dao.measurement_filters import MeasurementFilters
from src.dao.measurement_type import MeasurementType
from src.dto import db_service
from src.www import plot
from src.www import download


def server(input, output, session):
    """
    Tells shiny what should be dynamically rendered based on the various input and output components
    listed in the UI hierarchy defined above.
    """
    available_surface_types = db_service.get_all_surface_types()
    plot_xaxis_range: tuple[float, float] | None
    # Initialize measurement filters with default values
    filters = MeasurementFilters(
        measurement_type=MeasurementType.VI,
        surface_types=available_surface_types,
        vehicle_categories=[],  # Temporary arbitrary value,
        reference_speed=90,  # Most measurements are done on light vehicles at 90km/h
        surface_age_min_years=0,
        surface_age_max_years=20,
        campaign_date_min=db_service.get_oldest_measurement_date(),
        campaign_date_max=db_service.get_latest_measurement_date(),
        include_corrected_measurements=True,
    )
    # Queries all the available vehicle categories that user can chose from
    available_vehicle_categories = db_service.get_vehicle_categories_for_filters(
        filters
    )
    # Set the default vehicle category to VL
    filters.vehicle_categories = [available_vehicle_categories["VL"]]
    render_plot_listener = reactive.value(0)
    # Update campaign date range slider
    ui.update_date_range(
        id="date_range_campaign_date",
        start=filters.campaign_date_min.date(),
        end=filters.campaign_date_max.date(),
        min=filters.campaign_date_min.date(),
        max=filters.campaign_date_max.date(),
    )
    # Initialize surface type dorpdown
    ui.update_checkbox_group(
        id="surface_types",
        choices=filters.get_surface_type_names(),
        selected=filters.get_surface_type_names(),
    )

    def update_options_from_filters(filters: MeasurementFilters):
        """
        Updates the available options in the dropdown menus based on the current measurement filters
        Whenever possible, keep the currently selected option, or fallback to a default value

        Args:
            filters (MeasurementFilters): Current measurement filters
        """
        # Update available vehicle categories
        available_vehicle_categories = db_service.get_vehicle_categories_for_filters(
            filters
        )
        # Default to VL if previously selected vehicle is not available anymore
        if filters.measurement_type != MeasurementType.VI:
            filters.vehicle_categories = [available_vehicle_categories["VL"]]
        # Based on the updated filters, update the list of available reference speeds
        available_reference_speeds = (
            db_service.get_available_reference_speeds_for_filters(filters)
        )
        # Set the reference speed to the currently selected value if its available for the current filters,
        # otherwise set it to the lowest one available for current filters
        if filters.reference_speed not in available_reference_speeds:
            filters.reference_speed = available_reference_speeds[0]
        ui.update_select(
            id="dropdown_vehicle_category",
            choices=dict(
                zip(
                    list(available_vehicle_categories.keys()),
                    [v.longname for v in available_vehicle_categories.values()],
                )
            ),
            selected=[v.name for v in filters.vehicle_categories],
        )
        ui.update_select(
            id="dropdown_measurement_type", selected=filters.measurement_type.value
        )
        ui.update_select(
            id="dropdown_reference_speed",
            choices=[str(s) for s in available_reference_speeds],
            selected=str(filters.reference_speed),
        )
        ui.update_checkbox(
            id="show_dropdown_vehicle_category",
            value=filters.measurement_type == MeasurementType.VI,
        )
        ui.update_checkbox(
            id="show_switch_include_corrected_measurements",
            value=filters.measurement_type != MeasurementType.CPX,
        )

    @reactive.effect
    @reactive.event(input.btn_select_all)
    def on_click_select_all() -> None:
        """
        Triggered when clicking the Select all button. Selects all available road surface options.
        """
        nonlocal available_surface_types
        ui.update_checkbox_group(
            id="surface_types", selected=[s.name for s in available_surface_types]
        )

    @reactive.effect
    @reactive.event(input.btn_clear_selection)
    def on_click_clear_selection() -> None:
        """
        Triggered when clicking the Clear button. Deselects all selected road surface options.
        """
        ui.update_checkbox_group(id="surface_types", selected=[])

    @reactive.effect(priority=1)
    @reactive.event(
        input.dropdown_vehicle_category,
        input.dropdown_measurement_type,
        input.dropdown_reference_speed,
        input.slider_surface_age_range,
        input.date_range_campaign_date,
        input.switch_include_corrected_measurements,
        ignore_init=True,
    )
    def on_filters_update() -> None:
        """
        Listens to any change in the available filters and update UI in consequence
        """
        nonlocal filters
        # Update measurement type in filters
        filters.measurement_type = MeasurementType(input.dropdown_measurement_type())
        # Update vehicle category in filters
        filters.vehicle_categories = [
            available_vehicle_categories[v] for v in input.dropdown_vehicle_category()
        ]
        # Update reference speed in filters
        filters.reference_speed = int(input.dropdown_reference_speed())
        # Update surface age range in filters
        filters.surface_age_min_years, filters.surface_age_max_years = (
            input.slider_surface_age_range()
        )
        # Update campaign date range in filters
        filters.campaign_date_min, filters.campaign_date_max = (
            input.date_range_campaign_date()
        )
        # Update include corrected measurements
        filters.include_corrected_measurements = (
            input.switch_include_corrected_measurements()
        )
        # Update available options from filters
        update_options_from_filters(filters)
        # Trigger a graph refresh
        render_plot_listener.set(render_plot_listener.get() + 1)

    @render_widget  # type: ignore (shiny type checking is a bit funky at times)
    @reactive.event(
        input.dropdown_sorting_option,
        input.surface_types,
        input.switch_plot_auto_scale,
        input.number_plot_scale_min,
        input.number_plot_scale_max,
        render_plot_listener,
    )
    def box_plot() -> Figure:
        """
        Builds our box plot based on the selected surface types as input

        Returns:
            Figure: A Plotly box plot for the LA max values of selected surface types
        """
        nonlocal plot_xaxis_range
        # Get selected surfaces as input
        selected_surface_types = input.surface_types()
        filters.surface_types = list(
            filter(lambda s: s.name in selected_surface_types, available_surface_types)
        )
        # Set x axis range to either fixed (tuple) or auto (None)
        if input.switch_plot_auto_scale():
            plot_xaxis_range = None
        else:
            plot_xaxis_range = (
                input.number_plot_scale_min(),
                input.number_plot_scale_max(),
            )
        return plot.generate_plot_for_filters(
            filters, input.dropdown_sorting_option(), plot_xaxis_range
        )

    ####################
    # Download methods #
    ####################

    def get_dynamic_file_title() -> str:
        """
        Gets the dynamic part of the downloaded files titles based on the current filters.

        Returns:
            str: Dynamic part of file title for download
        """
        nonlocal filters
        vehicle_category = f"-{'|'.join([v.name for v in filters.vehicle_categories])}"
        if filters.measurement_type == MeasurementType.CPX:
            vehicle_category = ""
        title = (
            f"{filters.measurement_type.value.replace('/','-')}-"
            + f"{filters.reference_speed}kmh{vehicle_category}"
        )
        if filters.include_corrected_measurements:
            title += "-corrige"
        return title

    @render.download(
        filename=lambda: f"bruits-de-roulement-{get_dynamic_file_title()}.png"
    )
    async def button_download_plot():
        """
        Downloads the currently displayed plot as a PNG file.

        Yields:
            bytes: The PNG image as bytes
        """
        nonlocal plot_xaxis_range
        yield download.plot_to_img(
            plot.generate_plot_for_filters(
                filters, input.dropdown_sorting_option(), plot_xaxis_range
            )
        )

    @render.download(
        filename=lambda: f"bruits-de-roulement-{get_dynamic_file_title()}.csv"
    )
    async def button_download_plot_data():
        """
        Downloads the data shown in the box plot as a CSV file.
        It will include for each element: surface name, surface age, number of measurements,
        Q1, Q3, mean, median, upperfence and lowerfence values.

        Yields:
            str: Plot data as CSV
        """
        yield download.plot_data_csv(plot.compute_box_data(plot.get_plot_data(filters)))

    @render.download(
        filename=lambda: f"""bruits-de-roulement-{get_dynamic_file_title()}-brut.csv"""
    )
    async def button_download_raw_data():
        """
        Downloads the raw plot dataframe with surface type name, age and one row per measurement.

        Yields:
            str: Raw plot data as CSV
        """
        yield download.raw_data_csv(plot.get_plot_data(filters))

    # With this first set of filters, update the UI with the available options
    update_options_from_filters(filters)
