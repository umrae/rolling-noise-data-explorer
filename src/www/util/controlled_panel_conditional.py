from shiny import ui


def controlled_panel_conditional(*args, control_id: str, default_visibility: bool):
    """
    Shiny doesn't allow us to control the visibility of components directly
    so as a hack, we're using a dummy hidden checkbox that we can control
    programatically to update the visibility of our dropdown menu.

    To update the visibility of this component, use
    ```python
    shiny.ui.update_checkbox(id="your_control_id", value=True/False)
    ```

    Args:
        control_id (str): Id of the hidden checkbox that will be used to control the visibility of the given content
        is_hidden_by_default (bool): True if element should be visible by default, false otherwise

    Returns:
        Tag: The root ui.panel_conditional
    """
    return ui.panel_conditional(
        f"input.{control_id} === true",
        *args,
        ui.panel_conditional(
            "false", # Inner checkbox will always be hidden
            ui.input_checkbox(
                id=control_id,
                label= "",
                value=default_visibility
            )
        )
    )
