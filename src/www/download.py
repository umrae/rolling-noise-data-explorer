import pandas as pd

from plotly.graph_objects import Figure


def plot_to_img(plot: Figure) -> bytes:
    """
    Returns a PNG image of the given plot

    Args:
        plot (Figure): The plot to be converted as PNG

    Returns:
        bytes: The PNG image as bytes
    """
    return plot.to_image(
        format="png",
        engine="kaleido",
        scale=2.0,
        width=1000,
    )


def plot_data_csv(plot_data: pd.DataFrame):
    """
    Returns the data shown in the box plot (i.e. quantiles, median, mean, etc)

    Args:
        plot_data (DataFrame): Plot data (see plot.compute_box_data for details)

    Returns:
        str: Plot data as CSV
    """
    # Get rid of the measurment counts at the end of surface names
    plot_data["surface_name"] = plot_data["surface_name"].apply(
        lambda s: s.split(sep="(")[0][:-1]
    )
    # Don't include interfence range in export
    plot_data = plot_data.drop("interfence_range", axis=1)
    plot_data = plot_data.rename(
        columns={
            # Chose column names for export
            "surface_name": "Revêtement",
            "surface_category": "Catégorie revêtement",
            "measurements_count": "Nombre mesures",
            "lowerfence": "Moustache inférieure",
            "q1": "Q1",
            "median": "Médiane",
            "mean": "Moyenne",
            "q3": "Q3",
            "upperfence": "Moustache suppérieure",
            "date_min": "Mesure la plus ancienne",
            "date_max": "Mesure la plus récente",
        }
    )
    return plot_data.to_csv(index=False, float_format="%.1f")


def raw_data_csv(raw_data: pd.DataFrame):
    """
    Gets the raw plot dataframe with surface type name, age and one row per measurement as CSV.

    Args:
        raw_data (DataFrame): Raw plot data

    Returns:
        str: Raw plot data as CSV
    """
    # Get rid of the measurment counts at the end of surface names
    raw_data["surface_name"] = raw_data["surface_name"].apply(
        lambda s: s.split(sep="(")[0][:-1]
    )
    return raw_data.to_csv(index=False, float_format="%.1f")
