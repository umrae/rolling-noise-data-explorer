from shiny import ui
from shinyswatch import theme
from shinywidgets import output_widget
from faicons import icon_svg
from datetime import datetime

from src.dao.measurement_type import MeasurementType
from src.definitions import DATA_ROOT
from .sort_order import SortOrder
from .util.controlled_panel_conditional import controlled_panel_conditional


def layout():
    """
    Creates the layout of our app.
    Note: the identifiers of all components here should match the ones in the server definition

    Returns:
        Tag: A shiny `Tag` object with a `ui.page_fillable` as root
    """
    # Build and return our UI layout
    return ui.page_fluid(
        # Set our theme
        theme.litera(),
        # Somehow litera theme overrides font family only for <p> tags so we
        # need to explicitely set it back to the value provided by the theme
        ui.tags.style("p { font-family: var(--bs-body-font-family) }"),
        ui.div(
            ui.panel_title(
                ui.layout_columns(
                    ui.img(
                        src="https://www.umrae.fr/fileadmin/contributeurs/UMRAE/UMRAE-logo.png",
                        width=80,
                        style="display: block; margin-left: auto; margin-right: auto; object-fit: contain;",
                    ),
                    ui.h2("BDECHO : Base de Données d'Emissions sonores de CHaussées rOutières"),
                    ui.img(
                        src="https://geolittoral.din.developpement-durable.gouv.fr/telechargement/docs_communs/marianne_logo_cerema.png",
                        height="60",
                        style="object-fit: contain;",
                    ),
                    col_widths=[1, 9, 2],
                ),
                window_title="BDECHO",
            ),
            style="margin-top: 20px",
        ),
        # We use a sidebar layout with all the controls put in the sidebar and the
        # remaining space used for displaying our box graph
        ui.layout_sidebar(
            ui.sidebar(
                ui.panel_title("Options"),
                ui.input_select(
                    id="dropdown_sorting_option",
                    label="Trier par :",
                    choices={
                        SortOrder.ALPHABETICAL_ASC.value: "Revêtement A-Z",
                        SortOrder.ALPHABETICAL_DESC.value: "Revêtement Z-A",
                        SortOrder.MEDIAN_ASC.value: "Médiane croissante",
                        SortOrder.MEDIAN_DESC.value: "Médiane décroissante",
                    },
                    selected=SortOrder.MEDIAN_DESC.value,
                ),
                ui.input_switch(
                    id="switch_plot_auto_scale", label="Echelle automatique", value=True
                ),
                ui.panel_conditional(
                    "input.switch_plot_auto_scale === false",
                    ui.layout_columns(
                        ui.input_numeric(
                            id="number_plot_scale_min", label="Min", value=65.0
                        ),
                        ui.input_numeric(
                            id="number_plot_scale_max", label="Max", value=90.0
                        ),
                    ),
                ),
                ui.accordion(
                    ui.accordion_panel(
                        "Revêtements de chaussée",
                        ui.layout_columns(
                            ui.input_action_button(
                                id="btn_clear_selection",
                                label="Clear",
                                class_="btn-light",
                            ),
                            ui.input_action_button(
                                id="btn_select_all",
                                label="Select all",
                                class_="btn-dark",
                            ),
                        ),
                        ui.input_checkbox_group(
                            id="surface_types",
                            label="",
                            choices={},
                        ),
                    ),
                    ui.accordion_panel(
                        "Filtres",
                        ui.input_select(
                            id="dropdown_measurement_type",
                            label="Type de mesures",
                            choices={
                                MeasurementType.VI.value: "VI (Véhicules Isolés)",
                                MeasurementType.VM.value: "VM (Véhicules Maîtrisés)",
                                MeasurementType.VI_VM.value: "VI et VM",
                                MeasurementType.CPX.value: "CPX (Proximité en Continu)",
                            },
                            selected=MeasurementType.VI.value,
                        ),
                        controlled_panel_conditional(
                            ui.input_select(
                                id="dropdown_vehicle_category",
                                label=ui.span(
                                    "Catégorie de véhicule",
                                    ui.br(),
                                    "(Ctrl+click pour sélection multiple)",
                                ),
                                choices={},
                                multiple=True,
                                size="3",
                            ),
                            control_id="show_dropdown_vehicle_category",
                            default_visibility=True,
                        ),
                        ui.input_select(
                            id="dropdown_reference_speed",
                            label="Vitesse de référence (km/h)",
                            choices={},
                        ),
                        controlled_panel_conditional(
                            ui.input_switch(
                                id="switch_include_corrected_measurements",
                                label=ui.span(
                                    "Inclure mesures corrigées ",
                                    ui.tooltip(
                                        icon_svg("circle-question"),
                                        """
                                        Si cette option est activée, les mesures effectuées à des vitesses proches de
                                        la vitesse de référence seront également inclues dans les résultats. Les valeurs
                                        mesurées seront alors ramenées à la vitesse de référence choisie en utilisant une
                                        correction linéaire.
                                        """,
                                    ),
                                ),
                                value=True,
                            ),
                            control_id="show_switch_include_corrected_measurements",
                            default_visibility=True,
                        ),
                        ui.input_slider(
                            id="slider_surface_age_range",
                            label="Âge du revêtement (années)",
                            min=0,
                            max=20,
                            value=[0, 20],
                            drag_range=False,
                            ticks=True,
                        ),
                        ui.input_date_range(
                            id="date_range_campaign_date",
                            label="Date de mesure",
                            start=datetime.fromtimestamp(0),
                            end=datetime.now(),
                            separator=" - ",
                            language="fr",
                            startview="decade",
                        ),
                    ),
                    open=["Filtres"],
                ),
                width=320,
            ),
            ui.navset_tab(
                ui.nav_panel(
                    "Accueil", ui.markdown(open(f"{DATA_ROOT}/presentation.md").read())
                ),
                ui.nav_panel(
                    "Résultats",
                    # To make it interractible, we place our plot in a shiny widget instance
                    output_widget(
                        id="box_plot",
                    ),
                    ui.layout_columns(
                        ui.download_button(
                            id="button_download_plot",
                            label="Graph PNG",
                            icon=icon_svg("download"),
                            class_="btn-dark",
                        ),
                        ui.download_button(
                            id="button_download_plot_data",
                            label="Données graph CSV",
                            icon=icon_svg("download"),
                            class_="btn-light",
                        ),
                        ui.download_button(
                            id="button_download_raw_data",
                            label="Données brutes CSV",
                            icon=icon_svg("download"),
                            class_="btn-light",
                        ),
                        gap=80,
                        style="margin-top: 40px",
                    ),
                ),
                ui.nav_panel(
                    "Informations",
                    ui.markdown(open(f"{DATA_ROOT}/information.md").read()),
                ),
            ),
        ),
    )
