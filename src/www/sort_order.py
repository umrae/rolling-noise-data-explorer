from enum import Enum


class SortOrder(Enum):
    """
    Lists the various x axis sorting options for the box plot
    """

    ALPHABETICAL_ASC = "a_z"
    ALPHABETICAL_DESC = "z_a"
    MEDIAN_ASC = "median_up"
    MEDIAN_DESC = "median_down"
