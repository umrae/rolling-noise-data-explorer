import numpy as np
import plotly.express as px
import pandas as pd

from datetime import datetime
from collections import Counter
from plotly.graph_objs._figure import Figure
from src.dao.surface_category import SurfaceCategory
from src.dto import db_service
from src.dao.measurement_filters import MeasurementFilters
from src.www.sort_order import SortOrder


def get_plot_title(measurement_count: int, filters: MeasurementFilters) -> str:
    """
    Builds the plot title string based on the measurement filters currently in use

    Args:
        measurement_count (int): Number of measurements used to build the plot
        filters (MeasurementFilters): Filters in use (measurement type, vehicle category, reference speed)

    Returns:
        str: The title of the boxplot
    """
    return (
        f"<b>Base de données revêtements: {measurement_count} mesures "
        + f"{filters.measurement_type.value} sur {'|'.join([v.name for v in filters.vehicle_categories])}, "
        + f"au {filters.campaign_date_max.strftime('%d/%m/%Y')}<br>"
        + f"(LAmax, vitesse {filters.reference_speed} km/h, "
        + f"revêtements de {filters.surface_age_min_years} à {filters.surface_age_max_years} ans)</b>"
    )


def get_plot_data(filters: MeasurementFilters) -> pd.DataFrame:
    """
    Fetches the measurements in the database for the given filters and
    builds a pandas DataFrame object from the results

    Args:
        filters (MeasurementFilters): A set of filters for measurements

    Returns:
        DataFrame: A DataFrame object with the surface names and associated measurements
    """
    # Get all measurements made on those surfaces
    measurements = db_service.get_measurements_for_filters(filters)
    # Count the number of values per surface name
    counter = Counter([m.surface_type.name for m in measurements])
    # Populate our data frame by converting each measurement as a tuple
    return pd.DataFrame.from_records(
        data=list(
            map(
                lambda m: (
                    # Include the measurement count for this surface in the surface name
                    # This will simplify sorting operations later on when manipulating the dataframe
                    f"{m.surface_type.name} ({counter[m.surface_type.name]})",
                    m.surface_age,
                    m.surface_type.category.name,
                    # Correct the measured LAMax value using regression coeff.
                    # If the measurement was made at the same reference speed, this will have no impact
                    m.la_max
                    + m.regression_coeff
                    * np.log10(filters.reference_speed / m.reference_speed),
                    m.date,
                ),
                measurements,
            )
        ),
        columns=[
            "surface_name",
            "surface_age_in_years",
            "surface_category",
            "la_max",
            "date",
        ],
    )


def get_x_axis_labels(sorting_option_value: str, plot_data: pd.DataFrame) -> list[str]:
    """
    Determines the order of x axis labels based on the selected sorting method

    Args:
        sorting_option_value: Selected sorting option as str
        plot_data (DataFrame): Plot data in the form of a dataframe

    Returns:
        list[str]: Input labels sorted depending on the selected sorting method
    """
    # Group data in dataframe by surface name and compute the median of la_max for each surface type
    grouped_data = (
        plot_data[["surface_name", "la_max"]]
        .groupby(by=["surface_name"])
        .median()
        .reset_index()
    )
    # Determine the order in which surfaces should be put in the box plot x axis
    match sorting_option_value:
        case SortOrder.ALPHABETICAL_ASC.value:
            grouped_data.sort_values(by="surface_name", inplace=True, ascending=True)
        case SortOrder.ALPHABETICAL_DESC.value:
            grouped_data.sort_values(by="surface_name", inplace=True, ascending=False)
        case SortOrder.MEDIAN_ASC.value:
            grouped_data.sort_values(by="la_max", inplace=True, ascending=True)
        case SortOrder.MEDIAN_DESC.value:
            grouped_data.sort_values(by="la_max", inplace=True, ascending=False)
        case _:
            # Should never occur
            return []
    # Return the sorted surface names
    return grouped_data["surface_name"].to_list()


def compute_box_data(raw_data: pd.DataFrame) -> pd.DataFrame:
    data: list[tuple] = []
    for name, group in raw_data.groupby(by="surface_name"):
        la_max_group = group["la_max"]
        [category] = group["surface_category"].unique()
        q1, median, q3 = la_max_group.quantile(
            q=[0.25, 0.5, 0.75], interpolation="higher"
        )
        mean = la_max_group.mean()
        # Lower and upper fences (i.e. whiskers) span by default +/- 1.5 times
        # interquantile range (see https://plotly.com/python/reference/box/)
        # If the min or max value is below or above the fence, fallback to it
        interquantile_range = q3 - q1
        lower_fence = max(la_max_group.min(), q1 - 1.5 * interquantile_range)
        upper_fence = min(la_max_group.max(), q3 + 1.5 * interquantile_range)
        whiskers_range = upper_fence - lower_fence
        date_min = group["date"].min()
        date_max = group["date"].max()
        data.append(
            (
                name,
                category,
                whiskers_range,
                la_max_group.count(),
                lower_fence,
                q1,
                median,
                mean,
                q3,
                upper_fence,
                datetime.strftime(date_min, "%Y-%m-%d"),
                datetime.strftime(date_max, "%Y-%m-%d"),
            )
        )
    return pd.DataFrame.from_records(
        columns=[
            "surface_name",
            "surface_category",
            "interfence_range",
            "measurements_count",
            "lowerfence",
            "q1",
            "median",
            "mean",
            "q3",
            "upperfence",
            "date_min",
            "date_max",
        ],
        data=data,
    )


def generate_hover_trace(plot_data: pd.DataFrame) -> Figure:
    """
    Plotly doesn't allow felxible customization of hover data for box plots,
    so as a workaround we add a 0 opacity bar trace on top of our box plot and
    use this trace's data as custom hover data

    Args:
        plot_data (pd.DataFrame): Raw plot data

    Returns:
        Figure: Bar trace
    """
    # Compute quantiles, median and such from raw plot data
    hover_data = compute_box_data(plot_data)
    hover_data["base"] = hover_data.loc[:, "lowerfence"]
    hover_data.rename(
        inplace=True,
        columns={
            "measurements_count": "Mesures",
            "q1": "Q1",
            "q3": "Q3",
            "median": "Med",
            "mean": "Moy",
            "lowerfence": "Moustache inf",
            "upperfence": "Moustache sup",
            "date_min": "Date min",
            "date_max": "Date max",
        },
    )
    # Create custom bar trace with personalized hover template
    hover_plot = px.bar(
        data_frame=hover_data,
        x="interfence_range",
        y="surface_name",
        base="base",
        opacity=0,
        color="surface_category",
        color_discrete_map={
            SurfaceCategory.R1.name: SurfaceCategory.R1.plot_line_color,
            SurfaceCategory.R2.name: SurfaceCategory.R2.plot_line_color,
            SurfaceCategory.R3.name: SurfaceCategory.R3.plot_line_color,
            SurfaceCategory.UNDEFINED.name: SurfaceCategory.UNDEFINED.plot_line_color,
        },
        hover_data={
            "surface_name": False,
            "surface_category": False,
            "interfence_range": False,
            "base": False,
            "Mesures": ":d",
            "Q1": ":.1f",
            "Med": ":.1f",
            "Moy": ":.1f",
            "Q3": ":.1f",
            "Moustache inf": ":.1f",
            "Moustache sup": ":.1f",
            "Date min": True,
            "Date max": True,
        },
    )
    hover_plot.update_traces(showlegend=False)
    return hover_plot


def generate_plot_for_filters(
    filters: MeasurementFilters,
    sorting_option_value: str,
    xaxis_range: tuple[float, float] | None,
) -> Figure:
    """
    Builds our box plot based on the current filters

    Args:
        filters (MeasurementFilters): A set of filters for measurements

    Returns:
        Figure: A Plotly box plot for the LA max values of selected surface types
    """
    # Retrieve plot data from db as a dataframe
    plot_data = get_plot_data(filters)
    plot_data = plot_data.sort_values(by="surface_category")
    # Get sorted x axis labels
    x_axis_order = get_x_axis_labels(sorting_option_value, plot_data=plot_data)
    # Create interactive plot using plotly
    plot = px.box(
        data_frame=plot_data,
        x="la_max",
        y="surface_name",
        category_orders={"surface_name": x_axis_order},
        boxmode="overlay",
        color="surface_category",
        color_discrete_map={
            SurfaceCategory.R1.name: SurfaceCategory.R1.plot_line_color,
            SurfaceCategory.R2.name: SurfaceCategory.R2.plot_line_color,
            SurfaceCategory.R3.name: SurfaceCategory.R3.plot_line_color,
            SurfaceCategory.UNDEFINED.name: SurfaceCategory.UNDEFINED.plot_line_color,
        },
    )
    plot.update_layout(
        title=dict(
            text=get_plot_title(len(plot_data), filters),
            xanchor="center",
            x=0.5,
        ),
        margin=dict(t=80),
        xaxis_title="<b>LAmax en dBA</b>",
        yaxis_title="<b>Type de revêtement</b>",
        xaxis_tickformat=",.1f",
        plot_bgcolor="white",
        height=150 + 30 * plot_data["surface_name"].nunique(),
        legend=dict(traceorder="normal", title="<b>Catégorie</b>", orientation="h"),
        barmode="overlay",
        hovermode="y unified",
    )
    plot.update_yaxes(
        showgrid=True, gridwidth=1, gridcolor="LightGray", griddash="dash"
    )
    plot.update_xaxes(showgrid=True, gridwidth=1, gridcolor="LightGray", dtick=1.0)
    plot.update_traces(
        boxmean=True,
        hoverinfo="skip",
        hovertemplate=None,
    )
    # We disable hover info on boxplot in order to provide our own hover data
    plot.add_traces(generate_hover_trace(plot_data).data)
    # If auto scale is disabled, set a fixed user defined scale
    if xaxis_range:
        plot.update_xaxes(range=xaxis_range)
    # If nothing was selected, update the plot to show a help text
    if plot_data.empty:
        plot.add_annotation(
            text="Aucune donnée disponnible pour les filtres sélectionnés",
            x=2,
            y=2,
            showarrow=False,
            font=dict(size=16, color="crimson"),
        ).update_layout(showlegend=False, title="")
        plot.update_yaxes(visible=False)
        plot.update_xaxes(visible=False)
    return plot
