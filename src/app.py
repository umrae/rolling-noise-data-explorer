from shiny import App
from src.www.ui import layout
from src.www.server import server

# Start the shiny server with our app UI
app = App(
    layout(),
    server,
)
